
#Click on sceneviewer geo and jump to prim's material

import toolutils
import hou
import JDB_TK

def createViewerStateTemplate():
    template = hou.ViewerStateTemplate("MatPicker_obj", "MatPicker", hou.objNodeTypeCategory())
    template.bindFactory(JDB_TK.MatPicker)
    template.bindIcon("SOP_texture")
    return template
    

