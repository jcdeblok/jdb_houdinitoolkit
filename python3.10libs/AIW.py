import hou
import itertools
from collections.abc import Mapping
from collections import OrderedDict
import json
from enum import Enum
from pprint import pprint 
import numpy as np
import collections

import cProfile





DatasetSource_lookup=[hou.attribType.Point, hou.attribType.Prim, hou.attribType.Vertex, hou.attribType.Prim, hou.attribType.Point, hou.attribType.Vertex]
 

def assertAttrib(geo, attrib_type, name, default_val):

    findFunction=getattr(geo,f"find{attrib_type.name()}Attrib",False)

    attrib = findFunction(name)
    if not attrib:
        attrib = geo.addAttrib(attrib_type, name, default_val  )   
   
    return attrib 




def map_nested_dicts_modify(ob, func):
    for k, v in ob.items():
        if isinstance(v, Mapping):
            map_nested_dicts_modify(v, func)
        else:
            ob[k] = func(k,v)  


class EnumBF(Enum):
    
    def __init__(self, value):
        super().__init__()
 
    def __eq__(self, other):
        if isinstance(other, int):
            return self.value == other
        if isinstance(other, Enum):
            return self.value == other.value
        
        if isinstance(other, str):
            return self.name.lower() == other

        return False    
   
    @classmethod
    def _missing_(cls, name):
        for member in cls:
            if member.name.lower() == name.lower():
                return member
        else:
            return  

    def __int__(self):
        return self.value    

 
    def _name(self):
        return self.name.lower()    

    def __dict__(self):
        out={}
        for item in self:
            out[item.name]=item.value
        return out

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)
    


class DataType(EnumBF):
    In = 0
    Target = 1
    Output = 2

class NormalizeScope(EnumBF):
    Dataset = 0
    DatasetItem = 1
    Disabled = 2  

class NormalizePass(EnumBF):
    ComputeNormalization = 0
    ApplyNormalization = 1
    SkipNormalization = 2
    SinglePass = 3


class NormalizeCombine(EnumBF):
    Qualifier = 0
    Attribute = 1
    Combined = 2  
    Individual = 3  
    
class NormalizeMethod(EnumBF):
    CenterAndFit = 0
    Center = 1
    Fit = 2

class DatasetSource(EnumBF):
    Points= 0
    PointPrims= 1
    PointVerts= 2
    Prims = 3
    PrimPoints = 4
    PrimVerts = 5
 
class NNType(EnumBF):
    Regression = 0
    Convolution = 1



class DatasetBaseType(EnumBF):
    Points = 0
    Prims = 1

 

class AttribStats():
    def __init__(self, attrib=None):
        self.vmax=[]
        self.vmin=[]
        self.vmean=[]
        self.vcenter=[]
        self.attrib=attrib
        self.count=0
        self.attrib_size=0

        if attrib:
            self.attrib_size=attrib.size()
            self.getStats(attrib)

    def __add__(self, other):
       
        if len(other.vmax):

            if self.attrib_size==0 and other.attrib_size:
                 for i in range(0,other.attrib_size):
                    self.vmax.append(other.vmax[i] )
                    self.vmin.append(other.vmin[i] )
                    self.vmean.append( other.vmean[i] ) 
                    self.vcenter.append( other.vcenter[i] )


            for i in range(0,max(self.attrib_size, other.attrib_size )):
                self.vmax[i]=max(self.vmax[i], other.vmax[i])
                self.vmin[i]=min(self.vmin[i], other.vmin[i])
                self.vmean[i]=(self.vmean[i]*self.count + other.vmean[i]*other.count) / ( self.count + other.count  ) *0.5
                self.vcenter[i]=(self.vmax[i] + self.vmin[i]) * 0.5
            
            self.count+=other.count 
        
        return self

    def __radd__(self, other):
        if other == 0:
            return self
        else:
            return self.__add__(other)
  
    def offset(self, item):
        pass

    def toDict(self):
        out={
            "vmax": self.vmax,
            "vmin": self.vmin,
            "vmean": self.vmean,
            "vcenter": self.vcenter,
            "count": self.count,

            }
        return out
    
    
    def getStats(self, attrib):
        
        findFunction=getattr(attrib.geometry() ,f"{attrib.type().name().lower()}FloatAttribValues",False)
        attrib_size=attrib.size()
        res=list(findFunction(attrib.name()))
    
        self.count=len(res)/attrib_size
        
        for i in range(0,attrib_size):
            tmp=res[i::attrib_size]  
            if len(tmp):
                self.vmax.append(  max(tmp) )
                self.vmin.append(  min(tmp) )
                self.vmean.append(  sum(tmp)/ len(tmp) ) 
                self.vcenter.append( (max(tmp) + min(tmp)) * 0.5 )

        return self

class AIttrib():
    def __init__(self, attrib, tags=[]):

        
        skip_normalization=[]  #["Normal","Color", "Non-arithmetic"]

        self.attrib=attrib
        self.tags=[]

        if len(tags) == 0:
            tags.append("NoTag")

        self.addTags(tags)

        self.do_export=True
        self.allow_normalization=not attrib.qualifier() in skip_normalization
        self.node=None
        self.nominal_count = 0


    def addTags(self,tags):
 
        if tags:
            if type(tags)!=list:
                tags=[tags]

            #tmp=[]
            #for i,tag in enumerate(tags):
            #    n=f"{tag}_{self.attrib.size()}"
            #    if not n in tmp:
            #        tmp.append(n)


            self.tags=list(set(self.tags+tags))
                           

    def getKey(self, normalizeCombine):

        attrib_qualifier = self.attrib.qualifier() if self.attrib.qualifier() != "" else "None"

        if normalizeCombine == NormalizeCombine.Qualifier:
            return f"{attrib_qualifier}_{self.attrib.size()}"
            
        if normalizeCombine == NormalizeCombine.Attribute:
            return f"{self.attrib.name()}_{self.attrib.size()}"
        
        if normalizeCombine == NormalizeCombine.Combined:
            return f"aiw__global__{self.attrib.size()}"

        return False        
    
    def getStats(self):
        return AttribStats(self.attrib)
 

class AittribCollection():

    def __init__(self, main_attrib_type=None):
        
        if main_attrib_type:
            self.setMainAttribType(main_attrib_type)


        self.clear()

    def clear(self):
        self.aittribs = []
        self.has_error = False
        self.normalizeStats={}
        self.setNormalizeMethod(NormalizeMethod.CenterAndFit)
        self.setNormalizeMethod(NormalizeMethod.CenterAndFit)
        
        self.setNormalize_per_channel(False)
        self.normalize_range=[-1,1]
        self.no_upscale=False

    def setNormalizeScope(self,n):
        self.normalizeScope=n 

    def setMainAttribType(self, n):
        self.main_attrib_type=n

    def setNormalizeMethod(self, n, no_upscale=-1):
        self.normalizeMethod=n 
        if no_upscale!=-1:
            self.no_upscale=no_upscale

    def setNormalize_per_channel(self, n):
       self.normalize_per_channel=n 

    def setNormalize_range(self, n):
       #print("Not going ot be implemented, [-1,1 it is.")
       #self.normalize_range=n 
       pass

    def addFromString(self, node, attrib_type, names, tags=[], ignore_missing=True, no_export=False ):
        
        if node:
           
            geo=node.geometry()
        
            findFunction=getattr(geo,f"find{attrib_type.name()}Attrib",False)
        
            has_error=False
        
            for name in (self.sepString(names) if type(names)==str else names):
                    
                    attrib=findFunction(name)
                    if attrib:
                        
                        if not self.hasAttrib(attrib):

                            aittrib=AIttrib(attrib, tags=tags)
                            aittrib.node=node
                            aittrib.do_export=not no_export
                           
                            self.aittribs.append(aittrib)
                    else:
                        self.has_error=True
            
            if not ignore_missing and self.has_error:
                self.aittribs=[]
 
        return self.aittribs   

    def hasAttrib(self,attrib):
        out = self.getStats(filterFunc=lambda aittrib : aittrib.attrib==attrib )    
        return out

    def getNames(self):
        out= list(set([x.getKey(NormalizeCombine.Attribute) for x in self.aittribs if x.do_export==True ]))
        return out

    def getSizes(self):
        out= list(set([f"aiw__global__{x.attrib.size()}" for x in self.aittribs ]))
        return out

    def getNodes(self):
        out= list(set([x.node for x in self.aittribs ]))
     
        return out

    def getQualifiers(self):
        out= list(set([x.getKey(NormalizeCombine.Qualifier) for x in self.aittribs if x.do_export==True ]))
        return out

    def getTags(self):
        
        out=[]
        for attrib in self.aittribs:
            out+=attrib.tags
        out=list(set(out))
        return out

    def getStats(self, filterFunc=lambda aittrib : True, as_dict=False ):
        
        out=sum( [x.getStats() for x in self.aittribs if filterFunc(x)] )

        if type(out)==AttribStats:
            if as_dict:
                out=out.toDict()
        else:
            out=False
        
        return out  
    
    def getStatsBySize(self, as_dict=False):
        out={}
        for item in self.getSizes():        

      
            out[item]=self.getStats(filterFunc=lambda aittrib: f"aiw__global__{aittrib.attrib.size()}" == item, as_dict = as_dict)
        return out

 
    def getStatsByQualifier(self, as_dict=False):
        out={}
        for item in self.getQualifiers():        
            out[item]=self.getStats(filterFunc=lambda aittrib: aittrib.getKey(NormalizeCombine.Qualifier) == item, as_dict = as_dict)
        return out

    def getStatsByName(self, as_dict=False):
        out={}
        for item in self.getNames():        
            out[item]=self.getStats(filterFunc=lambda aittrib: aittrib.getKey(NormalizeCombine.Attribute) ==item, as_dict = as_dict)
        return out
    


    def getAittribs(self, filterFunc=lambda aittrib : True ):
        out=[x for x in self.aittribs if filterFunc(x)]  
        return out  

    def getAittribsByQualifier(self, key=None):
        out={}
        for item in self.getQualifiers():        
            out[item]=self.getAittribs(filterFunc=lambda aittrib: aittrib.getKey(NormalizeCombine.Qualifier) == item )
        if key:
            if key in out:
                return out[key]
            return []
        
        return out

    def getAittribsByName(self, key=None):
        out={}
        for item in self.getNames():        
            out[item]=self.getAittribs(filterFunc=lambda aittrib: aittrib.getKey(NormalizeCombine.Attribute) ==item )
        if key:
            if key in out:
                return out[key]
            return []
        
        return out
    
    def getAittribsBySize(self, size, key=None):
        out={}
        
        out=self.getAittribs(filterFunc=lambda aittrib: aittrib.attrib.size() == size )
        if key:
            if key in out:
                return out[key]
            return []
        
        return out

    
    def getAittribsByDataKey(self, data_key, key=None):
        out={}
        
        out=self.getAittribs(filterFunc=lambda aittrib: f"{aittrib.attrib.name()}_{aittrib.attrib.size()}" == data_key and aittrib.do_export )
        if key:
            if key in out:
                return out[key]
            return []
        
        return out

    def getAittribsByTags(self, key=None):
        out={}
        for item in self.getTags():        
            out[item]=self.getAittribs(filterFunc=lambda aittrib: item in aittrib.tags )
        
        if key:
            if key in out:
                return out[key]
            return []

        return out

    def getAittribsByNode(self, key=None):
        out={}
        for item in  self.getNodes() :   
            out[item.path()]=self.getAittribs(filterFunc=lambda aittrib: item == aittrib.node )

        if key:
            if key.path() in out:
         
                return out[key.path()]
            return []
    
        return out

    def sepString(self, s, dlim=' '):
        return filter(None,[x.strip() for x in s.split(dlim)]) 


    def computeNormalizeStats(self):
        self.normalizeStats[NormalizeCombine.Combined.name]=self.getStatsBySize(as_dict = True) 
        self.normalizeStats[NormalizeCombine.Qualifier.name]=self.getStatsByQualifier(as_dict = True)
        self.normalizeStats[NormalizeCombine.Attribute.name]=self.getStatsByName(as_dict = True)
        return self.normalizeStats
    
    def setNormalizeStats(self, data):
         self.normalizeStats=data


    def setWorkingSet(self, aittribs_set):
        for item in self.aittribs:
        
            item.do_export=item in aittribs_set
             
    def resetWorkingSet(self):
        for item in self.aittribs:
            item.do_export=True

    def getWorkingSet(self):
        return  [x for x in self.aittribs if x.do_export] 

    def getNormalizeMeta(self):
        out={
            "per_channel": self.normalize_per_channel,
            "range": self.normalize_range,
            "NormalizeMethod": self.normalizeMethod.value,
            "stats": dict(self.normalizeStats)
        }
        return out

    def getWorkingSetDataMap(self):
        out=[]

        for aittrib in self.aittribs:
            if  aittrib.do_export:
                sub={}
                attrib=aittrib.attrib
                sub["attrib"]=attrib.name()
                sub["size"]=attrib.size()
                sub["qualifier"]=attrib.qualifier()
                sub["type"]=attrib.type().name()
                sub["nominal_count"] =  aittrib.nominal_count

                out.append(sub)
        return out

 

 

    def getData( self, items,  normalizeCombine   ):
        data=[]
        keys=[]
       
        items=np.array(items).flatten().tolist()

        for aittrib in self.aittribs:

            if aittrib.do_export:

                key=aittrib.getKey(normalizeCombine)

                attrib=aittrib.attrib
                attrib_size=attrib.size()

                for item in items:
                    val=item.attribValue(attrib)
                    val=list(val) if attrib_size>1 else [val]
                    data.append(val)
                    keys.append(key)

                #data=[ [x,y,z], [x,y,z] ]

        return data, keys                    

 

class DataSet():
    def __init__(self):
        self.data=[]
        self.data_origin=[]
        self.node=None
        self.stats={}
        self.normalizeData=[]
        self.meta={}


    def create(self, node, aittribCollection, dataset_src, normalizeCombine = NormalizeCombine.Combined , xforms_node=[] ):

        #dataset_index_override = use attrib value to combine prims
        self.data_origin=[]
        self.data_center_id=[]
        self.data=collections.defaultdict( lambda: {"data":[]} )
        self.node=node
        self.aittribCollection=aittribCollection
        geo=node.geometry()

        dataset_type = self.aittribCollection.main_attrib_type 
 
        pointcount=geo.intrinsicValue("pointcount")
        primcount=geo.intrinsicValue("primitivecount")

        self._pointcache=[None] * pointcount


        #points
        if dataset_type==hou.attribType.Point:

            self.is_convo=geo.findPointAttrib("AIW_conv") != None   
            nnType=NNType.Convolution if self.is_convo else NNType.Regression
            self.convo_src=[None]*pointcount

            center_id_attr = geo.findPointAttrib(f"AIW_center")



            for point in geo.points():

                dataset_items, data, keys= collections.defaultdict(list), [], []

                conv=[]
                convo_abs=  [(x+point.number()) for x in point.attribValue("AIW_conv")] if self.is_convo else []
    
                if dataset_src==DatasetSource.Points:
                    for data_src,conv_id in [point], zip([None] * pointcount) if nnType==NNType.Regression else zip(  [ geo.point(min(max(0, conv_item), pointcount-1) ) for conv_item in convo_abs], convo_abs ):    
                        if self.is_convo and conv_id!=None:
                            conv.append( conv_id  )
                        [ [data, keys][index].__iadd__(item) for index,item in enumerate( self.aittribCollection.getData(data_src, normalizeCombine )) ]
                
                if dataset_src==DatasetSource.PointPrims:   
                    for data_src,conv_id in point.prims(),zip([None] * pointcount)  if nnType==NNType.Regression else zip( [ geo.point(min(max(0, conv_item), pointcount-1) ).prims() for conv_item in convo_abs], convo_abs ):  
                        if self.is_convo and conv_id!=None:
                            conv.append( conv_id  )                          
                        [ [data, keys][index].__iadd__(item) for index,item in enumerate( self.aittribCollection.getData(data_src, normalizeCombine )) ]
                
                if dataset_src==DatasetSource.PointVerts:
          
                    for data_src,conv_id in point.vertices(),zip([None] * pointcount)  if nnType==NNType.Regression else zip( [ geo.point(min(max(0, conv_item), pointcount-1)).vertices() for conv_item in convo_abs], convo_abs ):    
                        if self.is_convo and conv_id!=None:
                            conv.append( conv_id  )                        
                        [ [data, keys][index].__iadd__(item) for index,item in enumerate( self.aittribCollection.getData(data_src, normalizeCombine )) ]

                #create dataset_items
                [ dataset_items[key].append(data[index]) for index,key in enumerate(keys) ]

                #add dataset_items to dataset
                [ self.data[x]["data"].append(dataset_items[x]) for x in dataset_items ]

                self.data_origin.append(point.number())   

                if center_id_attr:
                    self.data_center_id.append(point.attribValue(center_id_attr))   
                
         
         
            for  item in self.data:
              
                input_geo = xforms_node.geometry()
                xform_attr= xforms_node.geometry().findPointAttrib(f"AIW_xform_{item}")
                if xform_attr!= None:
                    k=   np.array_split(np.array( input_geo.pointFloatAttribValues(xform_attr.name()) ), len( input_geo.points()))
                    self.data[item]["xform"]= k
                    
                    for i,points in enumerate( self.data[item]["data"]):

                        m=hou.Matrix4( k[ self.data_origin[i] ])
                        self.data[item]["data"][i]=[hou.Vector3(x)*m if len(x)==3 else x for x in points]
                       

        #prims
        if dataset_type==hou.attribType.Prim:

            self.is_convo=geo.findPrimAttrib("AIW_conv") != None   
            nnType=NNType.Convolution if self.is_convo else NNType.Regression
            self.convo_src=[None]*pointcount

            center_id_attr = geo.findPrimAttrib(f"AIW_center")


            for prim in geo.prims():

                dataset_items, data, keys= collections.defaultdict(list), [], []
                conv=[]
                convo_abs=  [(x+prim.number()) for x in prim.attribValue("AIW_conv")] if self.is_convo else []

                if dataset_src==DatasetSource.Prims:
                    for data_src,conv_id in zip([prim], [None] * primcount) if nnType==NNType.Regression else zip( [ geo.prim(min(max(0, conv_item), primcount-1) ) for conv_item in convo_abs], convo_abs ):    

                        if self.is_convo and conv_id!=None:
                            conv.append( conv_id  )

                        [ [data, keys][index].__iadd__(item) for index,item in enumerate( self.aittribCollection.getData(data_src, normalizeCombine )) ]
                
                if dataset_src==DatasetSource.PrimPoints:   

                    for data_src, conv_id in zip( prim.points(), [None]*len(prim.points()))  if nnType==NNType.Regression else zip( [geo.prim(min(max(0, conv_item), primcount-1) ).points() for conv_item in convo_abs],  convo_abs ):    #  [   [ [point1,point2,pointN], [point1,point2,pointN] ],  [prim1_number,primN_number]  ]
  
                        if self.is_convo and conv_id!=None:
                            conv.append( conv_id  )
                        [ [data, keys][index].__iadd__(item) for index,item in enumerate( self.aittribCollection.getData(data_src, normalizeCombine )) ]
                
                if dataset_src==DatasetSource.PrimVerts:
                    
                    
                    for data_src, conv_id in zip( prim.vertices(), [None]*len(prim.vertices()))  if nnType==NNType.Regression else zip( [geo.prim(min(max(0, conv_item), primcount-1) ).vertices() for conv_item in convo_abs],  convo_abs ):    #  [   [ [point1,point2,pointN], [point1,point2,pointN] ],  [prim1_number,primN_number]  ]
        
                        if self.is_convo and conv_id!=None:
                            conv.append( conv_id  )

                        [ [data, keys][index].__iadd__(item) for index,item in enumerate( self.aittribCollection.getData(data_src, normalizeCombine )) ]

                #create dataset_items
                [ dataset_items[key].append(data[index]) for index,key in enumerate(keys) ]

                #add dataset_items to dataset
                [ self.data[x]["data"].append(dataset_items[x]) for x in dataset_items ]
                
                if self.is_convo:
                    self.convo_src[prim.number()]=conv
                
                self.data_origin.append(prim.number())      
                
                if center_id_attr:
                    self.data_center_id.append( prim.attribValue(center_id_attr) + len(prim.points()*int((len(conv)-1)/2) ))  


   #chunk =  getattr(hou, f"Vector{data_size}")( chunk )


            for  item in self.data:
     
                input_geo = xforms_node.geometry()
                xform_attr= xforms_node.geometry().findPrimAttrib(f"AIW_xform_{item}")
                
                #only xform attribs that have a corrosponding AIW_xfrom_<key> attrib 
                if xform_attr!= None:
                    k=   np.array_split(np.array( input_geo.primFloatAttribValues(xform_attr.name()) ), len(input_geo.prims()))
                    self.data[item]["xform"]= k
                
                    for i,prims in enumerate( self.data[item]["data"]):
                        m=hou.Matrix4( k[ self.data_origin[i] ])
                        self.data[item]["data"][i]=[ hou.Vector3(x)*m if len(x)==3 else x for x in prims]

                center_datasetitem_index= self.node.geometry().findPrimAttrib(f"AIW_xform_{item}")

     

        self.updateMeta()
        
        return self
    
    def updateMeta(self):

        neuron_count=0

        

   

        

        for data_key in self.data:
            
            aittrib=self.aittribCollection.getAittribsByDataKey(data_key)[0]


           
            r={}
            
            for i in self.data[data_key]['data']:

                k=f"{len(i)}"

                if k not in r:
                    r[k]=0

                else:
                    r[k]+=1

            cnt=int(max(r, key=r.get))
            aittrib.nominal_count=cnt
            neuron_count += cnt * aittrib.attrib.size()

            #print(f"data_key: {data_key} = { int(max(r, key=r.get))}")
   
            

        self.meta = {"size" : neuron_count }
        #print(self.meta)
 

 

def addError(geo, key, msg):
    
    error_attribName="aiw_error"
    has_error_attribName="aiw_has_error"

    attrib = geo.findGlobalAttrib(error_attribName)
    has_error_attrib = geo.findGlobalAttrib(has_error_attribName)
    data=geo.dictAttribValue(attrib)

    if not key in data:
        data[key]=[]

    data[key].append(msg)

    geo.setGlobalAttribValue(attrib, data)
    geo.setGlobalAttribValue(has_error_attrib, 1)


def tensorToAttrib(geo, output_dataset, meta={}):
     
    pointcount=geo.intrinsicValue("pointcount")
    primcount=geo.intrinsicValue("primitivecount")

    is_convo=geo.findPointAttrib("AIW_conv") != None   
    nnType=NNType.Convolution if is_convo else NNType.Regression
    

    meta=getModelMeta(output_dataset)

   
    mp=meta["mp"]

    datamap=meta["datamap_target"]


    dataset_type = mp["dataset_type"]
    main_attrib_type = hou.attribType.Prim if dataset_type else hou.attribType.Point
    main_type_name="prim" if dataset_type else "point"
    dataset_trg = DatasetSource(mp[f"dataset_item_target_src_{main_type_name}"])

    target_aittribs=[]
    normalize_lookup= NormalizeCombine(mp["normalize_combine"]).name
    margin = 1.0 #- mp["data_margin"]


    normalize_method = NormalizeMethod(   meta["normalize_target"]["NormalizeMethod"] )
    per_channel =  meta["normalize_target"]["per_channel"]  
    
    

    for trg in datamap:
        findFunction=getattr(geo,f"find{trg['type']}Attrib",False)

        print(findFunction("N"))
        print(trg['attrib'])

        attrib=findFunction(  trg['attrib']  )
        if attrib:
            aittrib=  AIttrib( attrib )
            aittrib.key = aittrib.getKey(mp["normalize_combine"])
            aittrib.stats=meta["normalize_target"]["stats"][normalize_lookup][aittrib.key]
            
            aittrib.stats["vmax"] = np.array(aittrib.stats["vmax"])
            aittrib.stats["vmin"] = np.array(aittrib.stats["vmin"])
            aittrib.stats["vcenter"] = np.array(aittrib.stats["vcenter"])
            aittrib.stats["vscale"] = ( aittrib.stats["vmax"] - aittrib.stats["vmin"] ) * 0.5  

            target_aittribs.append( aittrib)

            
            xform =  (output_dataset.findPointAttrib( f"AIW_xform_{aittrib.key}"))
        
            aittrib.xform_attr= xform

      

       

    for point_dataset_item in output_dataset.points():
        
        data = point_dataset_item.attribValue( "output_data" )
        data_origin= point_dataset_item.attribValue( "data_origin" )

        
        input_center = point_dataset_item.attribValue( "input_center" )
        input_scale = point_dataset_item.attribValue( "input_scale" )
        
   
        
        if main_attrib_type == hou.attribType.Prim:

            prim = geo.prim(data_origin)
      
            if prim:
                data_offset=0

                 
                if dataset_trg==DatasetSource.Prims:
                    p = prim
                    for aittrib in target_aittribs:
                        data_size=aittrib.attrib.size()
                        data_attrib=aittrib.attrib

                        chunk=np.array(data[data_offset:data_offset+data_size])
                         
                        chunk /=  aittrib.stats["vscale"]  
                        chunk +=  aittrib.stats["vcenter"]  
                        p.setAttribValue(data_attrib,tuple(chunk))
        
                        data_offset+=data_size
                

                if dataset_trg==DatasetSource.PrimPoints:
 
                    
                        


                    for p in prim.points():
                
                        norm_offset=0
                      
                        for aittrib in target_aittribs:
                            data_size=aittrib.attrib.size()
                            data_attrib=aittrib.attrib

                         
                            chunk=np.array(data[data_offset:data_offset+data_size]) 
                         
                            scale_chunk=np.array(data[data_offset:data_offset+data_size])



                            #margin, center, fit, xform

                            #chunk /= 

                            if len(chunk)!=data_size:
                             
                                continue
                            if False:
                                chunk *=  aittrib.stats["vscale"]  
                                chunk *=  1/(1-0.2)  
                                chunk -=  aittrib.stats["vcenter"]  
                            else:
                                #chunk *=  aittrib.stats["vscale"]  
                                #chunk *=  1/(1-0.2)  
                               
                            
                                #print(input_scale[data_offset:data_offset+data_size])
                                
                                
                                chunk *= np.array(input_scale[norm_offset:norm_offset+data_size]    )   
                                chunk -= np.array(input_center[norm_offset:norm_offset+data_size] )

                                if aittrib.xform_attr and len(chunk)==data_size:
                                    xform = hou.Matrix4(point_dataset_item.attribValue(  aittrib.xform_attr )).inverted()
                                    
                                    if data_size in [2,3,4]:
                                        chunk =  getattr(hou, f"Vector{data_size}")( chunk )
                                        chunk *= xform 
 
                               
                           
                            p.setAttribValue(data_attrib,tuple(chunk))


                            norm_offset+=data_size
                            data_offset+=data_size

                if dataset_trg==DatasetSource.PrimVerts:
                    for p in prim.vertices():
                        for aittrib in target_aittribs:
                            data_size=aittrib.attrib.size()
                            data_attrib=aittrib.attrib

                            chunk=np.array(data[data_offset:data_offset+data_size])
                            chunk *=  aittrib.stats["vscale"]  
                            chunk +=  aittrib.stats["vcenter"]  
                            p.setAttribValue(data_attrib,tuple(chunk))
            
                            data_offset+=data_size

         
        if main_attrib_type == hou.attribType.Point:
            point = geo.point(data_origin)
             
            if point:
                data_offset=0

                if dataset_trg==DatasetSource.Points:
                    p = point
                 
                    for aittrib in target_aittribs:
                        data_size=aittrib.attrib.size()
                        data_attrib=aittrib.attrib

                        chunk=np.array(data[data_offset:data_offset+data_size])
                        #chunk *=  aittrib.stats["vscale"]  
                        #chunk +=  aittrib.stats["vcenter"]  
                        p.setAttribValue(data_attrib,tuple(chunk))
        
                        data_offset+=data_size

                if dataset_trg==DatasetSource.PointPrims:
                    for p in point.points():
                        for aittrib in target_aittribs:
                            data_size=aittrib.attrib.size()
                            data_attrib=aittrib.attrib

                            chunk=np.array(data[data_offset:data_offset+data_size])
                            chunk *=  aittrib.stats["vscale"]  
                            chunk +=  aittrib.stats["vcenter"]  
                            p.setAttribValue(data_attrib,tuple(chunk))
            
                            data_offset+=data_size

                if dataset_trg==DatasetSource.PointVerts:
                    for p in point.vertices():
                        for aittrib in target_aittribs:
                            data_size=aittrib.attrib.size()
                            data_attrib=aittrib.attrib

                            chunk=np.array(data[data_offset:data_offset+data_size])
                            chunk *=  aittrib.stats["vscale"]  
                            chunk +=  aittrib.stats["vcenter"]  
                            p.setAttribValue(data_attrib,tuple(chunk))
            
                            data_offset+=data_size




#setPrimIntAttribValues

from collections.abc import Iterable

def flatten(xs):
    for x in xs:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            yield from flatten(x)
        else:
            yield x


def getDataMeta(dataset, comb_axis=True, skip_if_in_range=True, c=""):
    
    chk=[]

    data_meta=collections.defaultdict(dict)


    for item in dataset:  

        tmp=dataset[item]["data"]
        chk.append( len(tmp) )
        len_item_attr=len(tmp[0][0])

        dataset_item_min=np.array([ [np.array(a)[:,ax].min() for ax in list(range(0,len_item_attr)) ] for a in tmp   ])   #get max axis values per  datasetitem
        dataset_item_max=np.array([ [np.array(a)[:,ax].max() for ax in list(range(0,len_item_attr)) ] for a in tmp  ])   #get max axis values per  datasetitem
 

        dataset_max=np.array([dataset_item_max[:,x].max()  for x in range(0,len_item_attr)]) # max axis values for whole dataset
        dataset_min=np.array([dataset_item_min[:,x].min()  for x in range(0,len_item_attr)]) # max axis values for whole dataset
   
        data_meta[item]= {
            "dataset_item": [ dataset_item_min, dataset_item_max   ],
            "dataset": [dataset_min, dataset_max  ],
            "skip_normalization": max(dataset_max)<=1.0 and min(dataset_min)>-1.0 and skip_if_in_range,
            "comb_axis": comb_axis,
            "len": len(tmp)
        }

        #if comb_axis:  
        #    data_meta = getDataMetaUniform(dataset, data_meta   )
       
    return data_meta 




def getDataMetaUniform(dataset, data_meta, center , single_item=None ):
    
  

    for item in dataset:  

        if single_item!=None and item!=single_item:
         
            continue

        dataset_item_min, dataset_item_max = data_meta[item]["dataset_item"]

        tmp=dataset[item]["data"]
        len_item_attr=len(tmp[0][0])

        dataset_item_max_uniform=np.array([[max(x)]*len_item_attr for x in (dataset_item_max - center )] )
        dataset_item_min_uniform=np.array([[min(x)]*len_item_attr for x in (dataset_item_min - center )] )
        dataset_max_uniform=np.array([dataset_item_max_uniform[:,x].max()  for x in range(0,len_item_attr)]) # max axis values for whole dataset
        dataset_min_uniform=np.array([dataset_item_min_uniform[:,x].min()  for x in range(0,len_item_attr)]) # max axis values for whole dataset

        data_meta[item]["dataset_item_uniform"]=[dataset_item_min_uniform,dataset_item_max_uniform]
        data_meta[item]["dataset_uniform"]=[dataset_min_uniform,dataset_max_uniform]

    return data_meta


def storeToGeo(geo, input_dataset, target_dataset=None, meta={}, remove_dups=True, remove_conflicts=True, mode = 0 ):
    stats={}
     
    mp=meta["mp"]
 
    # normalize tree:

    input_data=input_dataset.data
    target_data=target_dataset.data if target_dataset else {}

 
    
    input_data_normalized={}
    target_data_normalized={}

    input_do_center=1 if (mp["dataset_input_normalization_method"]==0 or mp["dataset_input_normalization_method"]==1) and mp["input_normalize_scope"]!=2 else 0
    input_do_fit=0 if (mp["dataset_input_normalization_method"]==1 or mp["input_normalize_scope"]==2 ) else 1
    comb_axis = mp["normalize_per_channel"] == 0 or input_dataset.is_convo
    blast_out_of_range=mp["blast_out_of_range"]
    input_global_scope=mp["input_normalize_scope"]==0
 
    skip_if_in_range=mp["normalize_no_upscale"]

    data_margin=1 #mp["data_margin"]

    skip_target_normalize = mp["target_normalize_scope"]==1


    link_input_target=1-mp["normalize_separate"]

    if (mp["dataset_target_normalization_method"]==-1 and mp["target_normalize_scope"]!=2):
        target_do_center=input_do_center 
        target_do_fit=input_do_fit
    else:
        target_do_center = 1 if (mp["dataset_target_normalization_method"]==0 or mp["dataset_target_normalization_method"]==1) and mp["target_normalize_scope"]!=2 else 0                   
        target_do_fit=0 if (mp["dataset_target_normalization_method"]==1 or mp["target_normalize_scope"]==2) else 1
   
    

    chk=[]

    #Remove dataset_items that have a deviating count
    
    failed=set()
    extended=set()
    trimmed=set()
    do_extend=mp["allow_extend"]
    do_trim=mp["allow_trim"]

    for data_key in input_data.keys():

        attrib_name=data_key.split("_")[0]
        attrib_size=int(data_key.split("_")[1])
      
        data_meta=[x for x in meta["datamap_input"] if x["attrib"]==attrib_name and x["size"]==attrib_size][0]

        for index,data_sub_item in enumerate(input_data[data_key]["data"]):

            data_len=len(data_sub_item) 
            data_nom_len =  data_meta["nominal_count"] 
            modified=False
            

            if data_len < data_nom_len:
                data_sub_item+=[[ 0.88888888888888 if type(data_sub_item[0])!=int else 0 ]*attrib_size]*(data_nom_len - data_len )
                modified=True
                
                extended.add( input_dataset.data_origin[index])

            if data_len> data_nom_len  :
                data_sub_item=data_sub_item[0 :data_nom_len ]
                modified=True
                trimmed.add( input_dataset.data_origin[index])

            if modified:
                input_data[data_key]["data"][index]=data_sub_item 
                pass

    for data_key in target_data.keys():

        attrib_name=data_key.split("_")[0]
        attrib_size=int(data_key.split("_")[1])
      
        data_meta=[x for x in meta["datamap_target"] if x["attrib"]==attrib_name and x["size"]==attrib_size][0]

        for index,data_sub_item in enumerate(target_data[data_key]["data"]):

            data_len=len(data_sub_item) 
            data_nom_len =  data_meta["nominal_count"] 
            modified=False
           

            if data_len < data_nom_len:
                data_sub_item+=[[ 11111.0 if type(data_sub_item[0])!=int else 0 ]*attrib_size]*(data_nom_len - data_len )
                modified=True
                extended.add( target_dataset.data_origin[index])

            if data_len> data_nom_len  :
                data_sub_item=data_sub_item[0 :data_nom_len ]
                modified=True
                trimmed.add( target_dataset.data_origin[index])
    

            if modified:
                target_data[data_key]["data"][index]=data_sub_item 
                pass

    
    if False:
        for index in sorted(failed, reverse=True) :
            for item in input_data:
                del input_data[item]["data"][index]

            for item in target_data:
                del target_data[item]["data"][index]

            if  target_dataset.data_origin[index] in trimmed:
                trimmed.remove( target_dataset.data_origin[index])
            if  target_dataset.data_origin[index] in extended:
                extended.remove( target_dataset.data_origin[index])

    #eject if no data:
        
    if len(input_data )==0:
        addError(geo,"dataset", "Empty Input DataSet")
        return "Empty Input Data"
    
    if len(target_data )==0 and mode==0:
        addError(geo,"dataset", "Empty Target DataSet")
        return "Empty target Data"


    #eject if input/target dataset_item count doesnt match
    if not all(x == chk[0] for x in chk):
        addError(geo,"dataset", f"Input/Target count mismatch  {' : '.join(map(str, chk))}")
        return f"Input/Target count mismatch  {' : '.join(map(str, chk))}"
    
    #Data should be mosty clean from this point on.
            
    input_data_meta = getDataMeta(input_data, comb_axis=comb_axis, skip_if_in_range=skip_if_in_range, c="inp")
    target_data_meta = getDataMeta(target_data, comb_axis=comb_axis, skip_if_in_range=skip_if_in_range, c="trg")
   

    #combine stats so input and target uses same normalisation range
    global_data_meta=collections.defaultdict(dict)

    if link_input_target and mode == 0:
        for item in input_data_meta:
                if item in target_data_meta:
                    
                    tmp=target_data[item]["data"]
                    len_item_attr=len(tmp[0][0])
                    
                    input_dataset_item_min = (input_data_meta[item]["dataset_item"][0]) #
                    input_dataset_item_max = (input_data_meta[item]["dataset_item"][1]) #
                    target_dataset_item_min = (target_data_meta[item]["dataset_item"][0]) #
                    target_dataset_item_max = (target_data_meta[item]["dataset_item"][1]) #
                    
                    input_dataset_min = (input_data_meta[item]["dataset"][0]) #
                    input_dataset_max = (input_data_meta[item]["dataset"][1]) #
                    target_dataset_min = (target_data_meta[item]["dataset"][0]) #
                    target_dataset_max = (target_data_meta[item]["dataset"][1]) #

                    dataset_item_min, dataset_item_max  = np.minimum(input_dataset_item_min, target_dataset_item_min), np.maximum(input_dataset_item_max, target_dataset_item_max )
                    dataset_min, dataset_max    = np.minimum(input_dataset_min, target_dataset_min),  np.maximum(input_dataset_max, target_dataset_max)
              
                    global_data_meta[item] = {
                        "dataset_item": [ dataset_item_min, dataset_item_max ],
                        "dataset": [dataset_min, dataset_max  ],
                        "skip_normalization": max(dataset_max)<=1.0 and min(dataset_min)>-1.0 and skip_if_in_range,
                        "comb_axis": comb_axis,
                        "len": len(tmp)
                    }
                    
                    if comb_axis:  
                        dataset_item_max_uniform=np.array([[max(x)]*len_item_attr for x in dataset_item_max] )
                        dataset_item_min_uniform=np.array([[min(x)]*len_item_attr for x in dataset_item_min] )
                        dataset_max_uniform=np.array([dataset_item_max_uniform[:,x].max()  for x in range(0,len_item_attr)]) # max axis values for whole dataset
                        dataset_min_uniform=np.array([dataset_item_min_uniform[:,x].min()  for x in range(0,len_item_attr)]) # max axis values for whole dataset

                        global_data_meta[item]["dataset_item_uniform"]=[dataset_item_min_uniform,dataset_item_max_uniform]
                        global_data_meta[item]["dataset_uniform"]=     [dataset_min_uniform,dataset_max_uniform]


    input_centers=collections.defaultdict(list)
    input_scales=collections.defaultdict(list)
    
    input_dataset_item_size=0
    
     

    for  item_index,item in enumerate(input_data):  #item is attr / attr group

        norm_data_meta = global_data_meta  if link_input_target and item in global_data_meta else input_data_meta

        input_tmp=np.array(input_data[item]["data"])
        len_item=len(input_tmp[0])
        len_item_attr=len(input_tmp[0][0])

        input_dataset_item_size+=len_item_attr


        input_dataset_item_min = norm_data_meta[item]["dataset_item"][0]
        input_dataset_item_max = norm_data_meta[item]["dataset_item"][1]
        input_dataset_min = norm_data_meta[item]["dataset"][0]
        input_dataset_max = norm_data_meta[item]["dataset"][1]
            
        skip_input_normalization =  norm_data_meta[item]["skip_normalization"]
        center=(input_dataset_max) * 0.0
        input_centers[item] =  np.array( [ center.tolist() for x in range(0,len(input_tmp))] )

        #CENTER
        if input_do_center and not skip_input_normalization:
            
            if input_global_scope: #center dateset as whole
                center=(input_dataset_max+input_dataset_min) * 0.5
                #input_tmp-=center

                np.subtract(input_tmp, center, out=input_tmp, casting="unsafe")

                input_centers[item] =  np.array( [ center.tolist()   for x in range(0,norm_data_meta[item]["len"])] )

            else:  #center each dataset_item induvidually
                if mp["use_convo_core"]==0 or input_dataset.is_convo==0:
                    center =   (input_dataset_item_max + input_dataset_item_min)*0.5 
                   
                else:

                    print("XXX")    

                    center = np.array([q[input_dataset.data_center_id[i]] for i,q in enumerate(input_tmp)]) 


                   
                input_tmp =  np.array( [input_tmp[i]-x for i,x in enumerate(center )])
                input_centers[item] = center
            
        #FIT
        if input_do_fit and not skip_input_normalization :        
             
            suffix=""

            if comb_axis:
                norm_data_meta = getDataMetaUniform(input_data,  norm_data_meta, center, single_item=item )       
                suffix = "_uniform"

            if input_global_scope: #fit dateset as whole

                data_max = norm_data_meta[item][f"dataset{suffix}"][1] - (center if not comb_axis else 0)
                data_min = norm_data_meta[item][f"dataset{suffix}"][0] - (center if not comb_axis else 0)
                
                scale=np.maximum(np.abs(data_max), np.abs(data_min))
                #input_tmp/=scale

                np.divide(input_tmp, scale, out=input_tmp, casting="unsafe")
                

                input_scales[item] = np.array( [ scale.tolist() for x in range(0,norm_data_meta[item]["len"])] )

            else: #fit each dataset_item induvidually

                data_max= norm_data_meta[item][f"dataset_item{suffix}"][1] - (center if not comb_axis else 0)
                data_min= norm_data_meta[item][f"dataset_item{suffix}"][0] - (center if not comb_axis else 0)

                if not mp["use_convo_as_whole"] or input_dataset.is_convo==0:
                    
                    tmp=np.maximum(np.abs(data_max), np.abs(data_min))
                    input_tmp= np.array( [input_tmp[i]/(x) for i,x in enumerate(tmp)])
                    input_scales[item] = tmp

                else:
                    
                    data_max = norm_data_meta[item][f"dataset{suffix}"][1] 
                    data_min = norm_data_meta[item][f"dataset{suffix}"][0] 
                    tmp=np.maximum(np.abs(data_max), np.abs(data_min))
                    np.divide(input_tmp, tmp, out=input_tmp, casting="unsafe")
                    input_scales[item] = np.array( [ tmp.tolist() for x in range(0, norm_data_meta[item]["len"] )] )

        else:
            input_scales[item] = np.array( [ [1.0]* norm_data_meta[item]["len"] for x in range(0,len(input_tmp))] )

        input_data_normalized[item]=(np.nan_to_num(input_tmp ) ).tolist() 


    target_dataset_item_size=0

   

    for item in target_data:

        is_linked=link_input_target and item in global_data_meta
 
        
        norm_data_meta = global_data_meta  if is_linked else target_data_meta
        target_tmp=np.array(target_data[item]["data"])
     
        len_item=len(target_tmp[0])
        len_item_attr=len(target_tmp[0][0])

        target_dataset_item_size+=len_item_attr

        target_dataset_item_min = norm_data_meta[item]["dataset_item"][0]
        target_dataset_item_max = norm_data_meta[item]["dataset_item"][1]
        target_dataset_min = norm_data_meta[item]["dataset"][0]
        target_dataset_max = norm_data_meta[item]["dataset"][1]


        skip_target_normalization =  norm_data_meta[item]["skip_normalization"]
        center=(target_dataset_max) * 0.0  

        input_dataset_item_min = norm_data_meta[item]["dataset_item"][0]
        input_dataset_item_max = norm_data_meta[item]["dataset_item"][1]
        input_dataset_min = norm_data_meta[item]["dataset"][0]
        input_dataset_max = norm_data_meta[item]["dataset"][1]


        #CENTER
    
        if target_do_center and not skip_target_normalization:
            
            if input_dataset.is_convo==1 and input_global_scope == False:
                
                
                if mp["use_convo_core"]==0:
                    center = np.array([q[int((len_item-1)*0.5)] for q in target_tmp] ) if not is_linked else input_centers[item]
                    target_tmp =  np.array( [target_tmp[i]-x for i,x in enumerate(center )])
                   
                else:
                    center = input_centers[item]
                    target_tmp =  np.array( [target_tmp[i]-x for i,x in enumerate(center )])
                  
                     

            else:
                
                
                if  is_linked:
              
                    center=(input_dataset_max+input_dataset_min) * 0.5
                    target_tmp= np.array( [target_tmp[i]-x for i,x in enumerate(input_centers[item] )])
                    
                else:
                    center=(target_dataset_max+target_dataset_min) * 0.5
                    target_tmp-=center 
             
                    

        #FIT
        
        if target_do_fit and not skip_target_normalization:        
             
         
            suffix=""
            norm_data_meta = global_data_meta  if link_input_target and item in global_data_meta else target_data_meta
     
            if comb_axis:
                norm_data_meta = getDataMetaUniform(target_data,  norm_data_meta, center, single_item=item )       
                suffix = "_uniform"
        

            if mp["use_convo_core"]==0 or input_dataset.is_convo==0 or input_global_scope == True: #center dateset as whole
                
                #center=(input_dataset_max+input_dataset_min) * 0.5
 
    

                data_max = norm_data_meta[item][f"dataset{suffix}"][1] - (center if not comb_axis else 0)
                data_min = norm_data_meta[item][f"dataset{suffix}"][0] - (center if not comb_axis else 0)
            
                

                np.divide(target_tmp, np.maximum(np.abs(data_max), np.abs(data_min)) , out=target_tmp, casting="unsafe")
            

           
            else: #only if convo the option per dataset_item
            

                data_max = norm_data_meta[item][f"dataset_item{suffix}"][1] - (center if not comb_axis else 0)
                data_min = norm_data_meta[item][f"dataset_item{suffix}"][0] - (center if not comb_axis else 0)
              
                if mp["use_convo_as_whole"] and input_dataset.is_convo==1: 
                

                    scale= input_scales[item]
                    target_tmp=np.array( [target_tmp[i]/(x) for i,x in enumerate(scale)])

                else:
                  

                    scale=np.maximum(np.abs(data_max), np.abs(data_min))
                    target_tmp = np.array( [target_tmp[i]/(x) for i,x in enumerate(scale)])
        
        target_data_normalized[item]= (np.nan_to_num(target_tmp )   ).tolist()  
        
 

    
    
    raw=[ input_data_normalized[x]  for x in input_data_normalized ] 

    ct=len(raw)


    input_final = []
    for j in range(0,len(raw[0])):
        tmp=[]
        for i in range(0,ct):
            for q in raw[i][j] :
                tmp+=(q)
        input_final.append( tmp )

   


    if mode == 0:
        raw=[ target_data_normalized[x]  for x in target_data_normalized ] 
        ct=len(raw)

        target_final = []
        for j in range(0,len(raw[0])):
            tmp=[]
            for i in range(0,ct):
                for q in raw[i][j]:
                    tmp+=(q)
            target_final.append(tmp)

 

    tmp=[[input_centers[x]]  for x in input_centers]
    centers_final=np.reshape(np.array( [ list(flatten(zip(*list(zip(*tmp))[x]))) for x in range(0,len(tmp[0]))], dtype=object).flatten(), (-1, sum([input_centers[x][0].size for x in input_centers]) ))
 
    tmp=[[input_scales[x]]  for x in input_scales] 
    scales_final=np.reshape(np.array( [ list(flatten(zip(*list(zip(*tmp))[x]))) for x in range(0,len(tmp[0]))], dtype=object).flatten(), (-1, sum([input_scales[x][0].size for x in input_scales]) ))
 

   
   

    for index in range(0, len(input_final) ):

     
        point=False

        if mode == 0:
            if blast_out_of_range == False or ( max(target_final[index])<=1.0   and max(input_final[index])<=1.0  and min(target_final[index])>=-1.0  and min(input_final[index])>=-1.0  ):
                point=geo.createPoint() 
                point.setAttribValue("input_data", input_final[index] )
                point.setAttribValue("target_data", target_final[index] )
                point.setAttribValue("data_origin", input_dataset.data_origin[index] )
                
                point.setAttribValue("input_center", centers_final[index] )
                point.setAttribValue("input_scale", scales_final[index] )
                if input_dataset.is_convo:
                    point.setAttribValue("input_convo_src",  input_dataset.convo_src[input_dataset.data_origin[index]]  )

                
                if "xform" in input_dataset.data["P_3"]:
                    assertAttrib( geo, hou.attribType.Point, "AIW_xform_P_3", input_dataset.data["P_3"]["xform"][ input_dataset.data_origin[index]]  )    
                    point.setAttribValue("AIW_xform_P_3",  input_dataset.data["P_3"]["xform"][ input_dataset.data_origin[index]] )
                    



        if mode == 1:
            if blast_out_of_range == False or (  max(input_final[index])<=1.0 and min(input_final[index])>=-1.0  ):
                point=geo.createPoint() 
                point.setAttribValue("input_data", input_final[index] )
                point.setAttribValue("data_origin", input_dataset.data_origin[index] )
                point.setAttribValue("input_center", centers_final[index] )
                point.setAttribValue("input_scale", scales_final[index] )

            if "xform" in input_dataset.data["P_3"]:
                assertAttrib( geo, hou.attribType.Point, "AIW_xform_P_3", input_dataset.data["P_3"]["xform"][ input_dataset.data_origin[index]]  )    
                point.setAttribValue("AIW_xform_P_3",  input_dataset.data["P_3"]["xform"][ input_dataset.data_origin[index]] )


        
        if point:
            if input_dataset.data_origin[index] in trimmed :
                point.setAttribValue("data_trimmed", 1)

                if not do_trim:
                    point.setAttribValue("data_error", 1)

            if input_dataset.data_origin[index] in extended:

                point.setAttribValue("data_extended",1 )
                if not do_extend:
                    point.setAttribValue("data_error", 1)


    stats_text=f'''Dataset items: using { len(geo.points())-len(failed)} out of {len(geo.points())}
Model Input Layer Nodes: { len(input_final[0]) }
Model Output Layer Nodes: { len(target_final[0]) if mode == 0 else "--" }
Removed Dataset Items with deviating count: { len(failed) }
    '''    

    if len(input_final)==0:
        addError(geo,"dataset","No valid dataset items")

    return (stats_text)


def map_nested_dicts_modify(ob, func):
    for k, v in ob.items():
        if isinstance(v, Mapping):
            map_nested_dicts_modify(v, func)
        else:
            ob[k] = func(k,v)  

def getModelMeta(geo, meta_attribName = "aiw_model_metadata"):
    meta=False
    if geo.findGlobalAttrib(meta_attribName):
        meta=  geo.dictAttribValue(meta_attribName)
        map_nested_dicts_modify(meta, lambda k,v: list(v) if "Vector" in v.__class__.__name__ else v) #somehow a list of 2 or 3 items becomes a hou.Vector2/3
    else:
        print(f"Meta data not found: {meta_attribName}")

    return meta

def storeModelMeta(geo, meta, meta_attribName = "aiw_model_metadata"):
 
    geo.setGlobalAttribValue( assertAttrib( geo, hou.attribType.Global, meta_attribName, {} ) , meta)       

